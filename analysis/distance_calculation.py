import logging

import numpy as np


def calculate_levenshtein_distance(string_1: str, string_2: str) -> float:
    """
    Calculate the Levenshtein similarity

    :param string_1: str, the first string
    :param string_2: str, the second string
    :return: float
    """
    m, n = len(string_1), len(string_2)
    max_len = max(m, n)
    dp = [[0] * (n + 1) for _ in range(m + 1)]

    try:
        for i in range(m + 1):
            for j in range(n + 1):
                if i == 0:
                    dp[i][j] = j
                elif j == 0:
                    dp[i][j] = i
                elif string_1[i - 1] == string_2[j - 1]:
                    dp[i][j] = dp[i - 1][j - 1]
                else:
                    dp[i][j] = 1 + min(dp[i - 1][j],  # Deletion
                                       dp[i][j - 1],  # Insertion
                                       dp[i - 1][j - 1])  # Replacement
        distance = dp[m][n]
        similarity_percentage = 1.0 - (distance / max_len)
    except ZeroDivisionError:
        logging.error("A zero division error occured while calculating the "
                      "Levenshtein similarity", exc_info=True)
        return 0.0
    except Exception:
        logging.error("While calculating the Levenshtein similarity and error"
                      "occurred", exc_info=True)
        return 0.0
    return similarity_percentage


def calculate_damerau_levenshtein_distance(string_1: str, string_2: str) \
        -> float:
    """
    Calculate the Damerau-Levenshtein similarity

    :param string_1: str, the first string
    :param string_2: str, the second string
    :return: float
    """
    d = dict()
    len_str1, len_str2 = len(string_1), len(string_2)
    max_len = max(len_str1, len_str2)

    try:
        for i in range(-1, len_str1 + 1):
            d[(i, -1)] = i + 1

        for j in range(-1, len_str2 + 1):
            d[(-1, j)] = j + 1

        for i in range(len_str1):
            for j in range(len_str2):
                cost = 0 if string_1[i] == string_2[j] else 1
                d[(i, j)] = min(d[(i - 1, j)] + 1,  # Deletion
                                d[(i, j - 1)] + 1,  # Insertion
                                d[(i - 1, j - 1)] + cost)  # Substitution

                if (i > 0 and j > 0 and
                        string_1[i] == string_2[j - 1] and
                        string_1[i - 1] == string_2[j]):
                    d[(i, j)] = min(d[(i, j)],
                                    d[i - 2, j - 2] + cost)  # Transposition

        distance = d[len_str1 - 1, len_str2 - 1]
        similarity_percentage = 1.0 - (distance / max_len)
    except ZeroDivisionError:
        logging.error("A zero division error occurred while calculating"
                      "the Damerau-Levenshtein similarity", exc_info=True)
        return 0.0
    except Exception:
        logging.error("An error occurred while calculating the Damerau-"
                      "Levenshtein similarity", exc_info=True)
        return 0.0
    return similarity_percentage


def jaro_similarity(string_1: str, string_2: str) -> float:
    """
    Compute the Jaro similarity

    :param string_1: str, the first string
    :param string_2: str, the second string
    :return: float
    """
    len_str1, len_str2 = len(string_1), len(string_2)

    if len_str1 == 0 or len_str2 == 0:
        return 0.0

    try:
        match_distance = (max(len_str1, len_str2) // 2) - 1
        matches = 0
        transpositions = 0

        for i in range(len_str1):
            start = max(0, i - match_distance)
            end = min(i + match_distance + 1, len_str2)

            for j in range(start, end):
                if string_1[i] == string_2[j]:
                    matches += 1
                    if i != j:
                        transpositions += 1
                    break

        if matches == 0:
            return 0.0

        jaro = (matches / len_str1 + matches / len_str2 + (
                matches - transpositions) / matches) / 3

    except ZeroDivisionError:
        logging.error("A zero division error occurred while calculating the "
                      "Jaro similarity", exc_info=True)
        return 0.0
    except Exception:
        logging.error("An error occurred while calculating the Jaro "
                      "similarity", exc_info=True)
        return 0.0

    return jaro


def jaro_winkler_similarity(string_1, string_2, prefix_scale=0.1):
    """
    Calculate the Jaro-Winkler similarity

    :param string_1: str, the first string
    :param string_2: str, the second string
    :param prefix_scale: float,
    :return: float
    """
    try:
        jaro_sim = jaro_similarity(string_1, string_2)

        prefix_length = 0
        max_prefix_length = min(len(string_1), len(string_2), 4)

        for i in range(max_prefix_length):
            if string_1[i] == string_2[i]:
                prefix_length += 1
            else:
                break

        jaro_winkler = jaro_sim + prefix_length * prefix_scale * (1 - jaro_sim)
    except Exception:
        logging.error("An error occurred while calculating the Jaro-"
                      "Winkler similarity", exc_info=True)
        return 0.0
    return jaro_winkler


def calculate_cosine_similarity(checklist_tags: [str], sample_content: [str]) \
        -> float:
    """
    Calculate the cosine similarity based on two lists of unequal length

    :param checklist_tags: list(str), contains the tags for the given checklist
    :param sample_content: list(str), contains the tags for the given sample
    :return: float
    """
    vocab = {}
    i = 0

    # loop through each list, find distinct words and map them to a
    # unique number starting at zero

    for word in checklist_tags:
        if word not in vocab:
            vocab[word] = i
            i += 1

    for word in sample_content:
        if word not in vocab:
            vocab[word] = i
            i += 1
    # create a numpy array (vector) for each input, filled with zeros
    a = np.zeros(len(vocab))
    b = np.zeros(len(vocab))

    # loop through each input and create a corresponding vector for it
    # this vector counts occurrences of each word in the dictionary
    for word in checklist_tags:
        index = vocab[word]  # get index from dictionary
        a[index] += 1  # increment count for that index

    for word in sample_content:
        index = vocab[word]
        b[index] += 1

    try:
        # use numpy's dot product to calculate the cosine similarity
        sim = np.dot(a, b) / np.sqrt(np.dot(a, a) * np.dot(b, b))
    except ZeroDivisionError:
        logging.error("Calculating the cosine similarity resulted in a zero"
                      "division", exc_info=True)
        return 0.0
    except Exception:
        logging.error("An error occurred while trying to calculate the cosine"
                      "similarity", exc_info=True)
        return 0.0
    return sim
