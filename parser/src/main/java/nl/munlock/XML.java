package nl.munlock;

import com.mongodb.client.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;

import static com.mongodb.client.model.Projections.*;

public class XML {

    // Connect to MongoDB instance
    static MongoClient mongoClient;

    // Select the database and collection
    static MongoDatabase database;
    static MongoCollection<Document> mongoSamples;
    static MongoCollection<Document> mongoProjects;

    static HashSet<String> accessions = new HashSet<>();
    public static final Logger logger = LogManager.getLogger(XML.class);

    static void mongoInit(CommandOptions commandOptions) {
        // Set mongodb connection
        XML.mongoClient = MongoClients.create("mongodb://"+commandOptions.username+":" + commandOptions.password + "@"+commandOptions.host+":"+commandOptions.port+"/");
        // Select the database and collections
        database = mongoClient.getDatabase("bio");
        if (commandOptions.biosample != null && commandOptions.biosample.getAbsolutePath().contains("ddbj"))
            mongoSamples = database.getCollection("ddbj_samples");
        if (commandOptions.biosample != null && commandOptions.biosample.getAbsolutePath().contains("ncbi"))
            mongoSamples = database.getCollection("ncbi_samples");

        if (commandOptions.bioproject != null && commandOptions.bioproject.getAbsolutePath().contains("ddbj"))
            mongoProjects = database.getCollection("ddbj_projects");
        if (commandOptions.bioproject != null && commandOptions.bioproject.getAbsolutePath().contains("ncbi"))
            mongoProjects = database.getCollection("ncbi_projects");

        logger.info("Connected to MongoDB");
    }

    static void processBiosample(File biosample) {
        if (biosample == null) {
            return;
        }
        logger.info("For buffering purposes, we will first retrieve all biosample accessions from MongoDB to avoid duplicates");
        // Obtain all accessions from biosamples using document BioSample.accession
        try (MongoCursor<Document> results = mongoSamples.find().projection(fields(include("BioSample.accession"), excludeId())).iterator()) {
            while (results.hasNext()) {
                Object document = results.next().get("BioSample", Document.class).get("accession");
                accessions.add(document.toString());
                if (accessions.size() % 1000000 == 0) {
                    // Print uing a thousand separator
                    String accessionCounter = String.format("%,d", accessions.size());
                    System.err.println("Retrieved " + accessionCounter + " biosamples from MongoDB");
                }
            }
        }
        logger.info("Found " + accessions.size() + " biosample entries in MongoDB");

        try (
                // Create a FileInputStream to read the GZIP file.
                FileInputStream fis = new FileInputStream(biosample);
                // Wrap the FileInputStream in a GZIPInputStream to decompress the data.
                GZIPInputStream gis = new GZIPInputStream(fis);
                // Wrap the GZIPInputStream in an InputStreamReader to convert bytes to characters.
                InputStreamReader isr = new InputStreamReader(gis);
                // Wrap the InputStreamReader in a BufferedReader to read text data line by line.
                BufferedReader reader = new BufferedReader(isr)
        ) {
            String line;
            boolean isInsideBioSample = false;
            boolean skipSample = false;
            StringBuilder sampleXML = new StringBuilder();
            long sampleCounter = 0;
            List<Document> documents = new ArrayList<>();

            Pattern pattern = Pattern.compile("accession=\"(.*?)\"");
            String accession;
            while ((line = reader.readLine()) != null) {
                if (line.trim().startsWith("<BioSample ")) {
                    accession = getAccession(pattern, line);
                    skipSample = accessions.contains(accession);
                    sampleCounter++;
                    if (sampleCounter % 1000 == 0) {
                        // Sample counter with a thousand separator
                        String sampleCounterString = String.format("%,d", sampleCounter);
                        System.err.println("Processed " + sampleCounterString + " samples");
                    }
                    isInsideBioSample = true;
                    sampleXML = new StringBuilder(); // Reset for a new project
                    sampleXML.append(line).append(System.lineSeparator()); // Include the start tag
                    continue;
                }

                if (skipSample) {
                    continue;
                }

                if (isInsideBioSample) {
                    sampleXML.append(line).append(System.lineSeparator()); // Append current line

                    if (line.trim().endsWith("</BioSample>")) {
                        isInsideBioSample = false;
                        // Here you have the complete XML for a Project element in projectXml
                        // System.out.println(projectXml); // Process or store the project XML string as needed
                        JSONObject jsonObject = org.json.XML.toJSONObject(sampleXML.toString());
                        String jsonString = jsonObject.toString(4);
                        try {
                            documents.add(Document.parse(jsonString));
                            if (documents.size() >= 5000) {
                                System.err.println("Inserting " + documents.size() + " documents");
                                mongoSamples.insertMany(documents);
                                documents.clear();
                            }
                        } catch (NumberFormatException e) {
                            logger.error(e.getMessage());
                        }
                    }
                }
            }
            // Insert the remaining documents
            if (!documents.isEmpty()) {
                System.err.println("Inserting " + documents.size() + " documents");
                mongoSamples.insertMany(documents);
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    static void processBioproject(File bioproject) {
        if (bioproject == null) {
            return;
        }
        logger.info("For buffering purposes, we will first retrieve all bioproject accessions from MongoDB to avoid duplicates");
        MongoCursor<Document> results = mongoProjects.find().projection(fields(include("Project.ProjectID.ArchiveID.accession"), excludeId())).iterator();
        while (results.hasNext()) {
            Document result = results.next();
            Object document = result.get("Project", Document.class).get("ProjectID", Document.class).get("ArchiveID", Document.class).get("accession");
            accessions.add(document.toString());
            if (accessions.size() % 10_000 == 0) {
                // Print using a thousand separator
                String accessionCounter = String.format("%,d", accessions.size());
                System.err.println("Processed " + accessionCounter + " bioprojects");
            }
        }
        System.err.println("Found " + accessions.size() + " bioprojects entries in MongoDB");

        Pattern pattern = Pattern.compile("ArchiveID accession=\"(.*?)\"");
        List<Document> documents = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(bioproject))) {
            String line;
            boolean isInsideProject = false;
            boolean skipProject = false;
            StringBuilder projectXml = new StringBuilder();
            long projectCounter = 0;
            long lineCounter = 0;
            String accession;
            HashSet<String> accessionsProcessed = new HashSet<>();
            int payloadSize = 0;
            long skipCounter = 0;
            while ((line = reader.readLine()) != null) {
                lineCounter++;
                if (line.trim().startsWith("<Package>")) {
                    if (projectCounter % 100000 == 0)
                        System.err.println("Processed " + projectCounter + " projects");
                    skipProject = false;
                    projectCounter++;
                    isInsideProject = true;
                    projectXml = new StringBuilder(); // Reset for a new project
                    projectXml.append(line).append(System.lineSeparator()); // Include the start tag
                    continue;
                } else if (skipProject) {
                    continue;
                }

                if (line.contains("<ArchiveID")) {
                    accession = getAccession(pattern, line);
                    if (accessionsProcessed.contains(accession)) {
                        System.err.println("Duplicate accession " + accession + " found in line " + lineCounter);
                    } else {
                        accessionsProcessed.add(accession);
                    }
                    if (accession == null) {
                        throw new RuntimeException("No accession found in " + line);
                    }
                    if (accessions.contains(accession)) {
                        // Skip if the project already exists
                        skipProject = true;
                    }
                }

                if (isInsideProject) {
                    projectXml.append(line).append(System.lineSeparator()); // Append current line

                    if (line.trim().endsWith("</Package>")) {
                        isInsideProject = false;
                        JSONObject jsonObject = org.json.XML.toJSONObject(projectXml.toString());
                        jsonObject = (JSONObject) jsonObject.get("Package");
                        jsonObject = (JSONObject) jsonObject.get("Project");
                        String jsonString = jsonObject.toString(4);
                        try {
                            Document document = Document.parse(jsonString);
                            payloadSize += jsonString.length();
                            if (documents.isEmpty()) {

                            } else if (payloadSize > 16777216 || documents.size() > 1000) {
                                System.err.print("Inserting " + documents.size() + " documents with a payload size of " + payloadSize + " bytes\r");
                                mongoProjects.insertMany(documents);
                                documents.clear();
                                payloadSize = 0;
                            }
                            if (jsonString.length() > 16777216) {
                                System.err.println("Payload size exceeds 16MB: " + jsonString.length() + " bytes");
                                skipCounter++;
                            } else {
                                documents.add(document);
                            }
                        } catch (NumberFormatException e) {
                            logger.error(e.getMessage());
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getAccession(Pattern pattern, String xmlString) {
        Matcher matcher = pattern.matcher(xmlString);
        // if multiple hits are found, throw an exception
        Stream<MatchResult> results = matcher.results();
        List<MatchResult> matchResults = results.toList();
        if (matchResults.size() == 1) {
            return matchResults.getFirst().group(1);
        }
        if (matchResults.size() > 1) {
            throw new RuntimeException("Multiple accessions found in " + xmlString);
        }
        return null;
    }
}
