# Code to analyse the clustering results

# Variation detection
import json
from pathlib import Path

import pandas as pd

from variants import load_variants, set_logging, get_variants, get_terms, get_ignores

# Importing libraries

# Global variables
output_folder = Path("data")


def analyse_data():
    lookup = get_variants()
    all_terms = get_terms()
    ignore = get_ignores()
    # Loop through the distance folder in data
    checksum = ""
    files = sorted((output_folder / "distance").glob("*.tsv"))
    # Get full paths
    full_paths = [file.resolve() for file in files]
    del files
    groups = {}
    for file in full_paths:
        print(file)
        checksum = file.stem.split("_")[0]
        if checksum not in groups:
            groups[checksum] = []
        groups[checksum].append(file)
    # Loop through the groups
    for key, files in groups.items():
        print(f"Processing group {key}")
        # Load the data
        dfs = []
        for file in files:
            # Load first 10 rows and append to the data
            if file.stat().st_size == 0:
                continue
            df = pd.read_csv(file, sep="\t", nrows=5)
            method = file.stem.split("_")[-1].replace(".tsv", "")
            df['method'] = method
            # Only append non-empty DataFrames that do not consist entirely of NA values
            if not df.empty and not df.isna().all(axis=1).all():
                dfs.append(df)
        # Concatenate the data
        df = pd.concat(dfs, ignore_index=True)
        # Remove duplicate tag2 and keep 1
        df.drop_duplicates(subset=['tag2'], keep='first', inplace=True)
        if df.iloc[2, 0] in ignore:
            print(f"Skipping {df.iloc[2, 0]} as it is in the ignore list")
            continue
        if df.iloc[2, 0] in all_terms:
            print(f"Skipping {df.iloc[2, 0]} as it is in the all terms list")
            continue
        # Insert a new row at the top
        new = pd.DataFrame([["new", "tag", 1, None, None], ["ignore", "tag permanently", 1, None, None]])
        new.columns = df.columns
        df = pd.concat([new, df], ignore_index=True)
        # Reset the index
        df.reset_index(drop=True, inplace=True)

        # Ask the user to select the row number that matches the variant
        print("Select the row number that matches the variant")
        print(df.to_string())
        row = input("Enter the row number: ")
        try:
            row = int(row)
        except ValueError:
            print("Invalid row number")
            continue
        if row == 0:
            term = df.iloc[2][0]
            lookup[term] = {}
            lookup[term]['true_variants'] = []
            lookup[term]['false_variants'] = []
            print(f"Added '{term}' to lookup")
        elif row == 1:
            if 'ignore' not in lookup:
                lookup['ignore'] = {}
            if 'tags' not in lookup['ignore']:
                lookup['ignore']['tags'] = []
            lookup['ignore']['tags'].append(df.iloc[2, 0])
            print(f"Added '{df.iloc[2, 0]}' to ignore")
        else:
            # Get the variant
            variant = '\t'.join(map(str, df.iloc[row]))
            print(f"SELECTED: {variant}")
            # Add this to the lookup
            replace_term = df.iloc[row, 1]
            analysed_term = df.iloc[row, 0]
            if replace_term in lookup:
                if 'true_variants' not in lookup[replace_term]:
                    lookup[replace_term]['true_variants'] = []
                lookup[replace_term]['true_variants'].append(analysed_term)
                print(
                    f"Added '{analysed_term}' to '{replace_term}' as part of true variant: {lookup[replace_term]['true_variants']}")
            elif replace_term in all_terms:
                print(
                    f"Term found as part of all terms: '{replace_term}' and will be added to '{all_terms[replace_term]}'")
                lookup[all_terms[replace_term]]['true_variants'].append(analysed_term)
        # Write lookup to file
        json_file = Path("data") / "variants_corrected.json"
        with open(json_file, "w") as f:
            json.dump(lookup, f, indent=4)
        print(f"Updated lookup file with {len(lookup)} terms and written to {json_file}")


def main():
    set_logging("data", debug=False)
    load_variants(file_path=Path("data") / "variants_corrected.json")
    # Analyse the data
    analyse_data()
    # Get variants


if __name__ == '__main__':
    main()
