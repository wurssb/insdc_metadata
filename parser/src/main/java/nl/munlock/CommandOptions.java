package nl.munlock;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;


@Parameters(commandDescription = "Available options: ")

public class CommandOptions {
    public static final Logger logger = LogManager.getLogger(CommandOptions.class);

    @Parameter(names = {"-debug"}, description = "Debug mode")
    public boolean debug;

    @Parameter(names = {"-help"}, description = "Help overview")
    public boolean help;

    @Parameter(names = {"-bioproject"}, description = "Bioproject xml file")
    public File bioproject;

    @Parameter(names = {"-biosample"}, description = "Biosample xml file")
    public File biosample;

    // Mongodb username
    @Parameter(names = {"-username"}, description = "Mongodb username", required = true)
    public String username;

    // Mongodb password
    @Parameter(names = {"-password"}, description = "Mongodb password", required = true)
    public String password;

    // Mongodb host
    @Parameter(names = {"-host"}, description = "Mongodb host", required = true)
    public String host;

    // Mongodb port
    @Parameter(names = {"-port"}, description = "Mongodb port", required = true)
    public int port;

    // Mongodb database
    @Parameter(names = {"-database"}, description = "Mongodb database")
    public String database = "bio";

    // Mongodb collection
    @Parameter(names = {"-sample_collection"}, description = "Mongodb sample collection", hidden = true)
    public String sample_collection = "sample";

    // Mongodb collection
    @Parameter(names = {"-project_collection"}, description = "Mongodb project collection", hidden = true)
    public String project_collection = "project";


    public CommandOptions(String[] args) {
        logger.info("For startup options use -help");
        JCommander jc = JCommander.newBuilder().addObject(this).build();
        try {
            jc.parse(args);
        } catch (Exception e) {
            logger.error("Error parsing command line arguments: " + e.getMessage());
            jc.usage();
            System.out.println("  * required parameter");
            System.exit(1);
        }

        if (this.help) {
            jc.usage();
            System.out.println("  * required parameter");
            System.exit(0);
        }
    }

    public CommandOptions() {
    }
}
