import gzip
import xml.etree.ElementTree as ET
from pprint import pprint

import xmltodict


def parse_gzipped_xml(file_path, record_tag):
    print(f"Processing file: {file_path} with record tag: {record_tag}")
    with gzip.open(file_path, 'rt', encoding='utf-8') as gz_file:
        print("Opening file...")
        context = ET.iterparse(gz_file, events=("start", "end"))
        _, root = next(context)  # Get the root element

        for event, elem in context:
            if event == "end" and elem.tag == record_tag:
                # Process the record
                yield elem

                # Clear the element to free memory
                root.clear()
    print("Done!???")
def main():
    print("Parsing BioSample records...")
    with gzip.open("lookup.tsv.gz", "wt") as gzip_output:
        gzip_output.write("Accession\tLast Update\tKey\tValue\n")
        for index, record in enumerate(parse_gzipped_xml("/Users/koeho006/Downloads/biosample_set.xml.gz", "BioSample")):
            if index % 10_000 == 0:
                print(f"Processing record {index}")
            # Convert the record to a dictionary or process it as needed
            xml_content = ET.tostring(record, encoding='unicode')
            # XML to JSON dict
            json_dict = xmltodict.parse(xml_content)
            # pprint(json_dict, indent=4)
            last_update = json_dict['BioSample']['@last_update']
            accession = json_dict['BioSample']['@accession']
            try:
                attributes = json_dict['BioSample']['Attributes']['Attribute']
                for attribute in attributes:
                    value = attribute['#text'].replace("\n", " ").strip().replace("\t", " ")
                    key = ""
                    if '@harmonized_name' in attribute:
                        key = attribute['@harmonized_name']
                    else:
                        key = attribute['@attribute_name']
                    print(f"{accession}\t{last_update}\t{key}\t{value}", file=gzip_output)
            except TypeError:
                # print(f"Record {accession} has no attributes")
                continue
            except KeyError:
                # print(f"Record {accession} has no attributes")
                continue

                
if __name__ == "__main__":
    main()