# INSDC_metadata

This repository contains the code used to process the biosample and bioproject data from DDBJ/ENA/GenBank (INSDC) into a mongodb database.
Each biosample and bioproject record is stored as a document in the database.

## Parser

In the parser folder you will find a java project that processes the XML files from DDBJ into individual documents and stores this into mongodb.

The code tests can be executed (and is tested in intellij) either through the command line or through the IDE.

To compile the code and use the jar for execution `./gradlew build -x test` and then `java -jar build/libs/insdc.jar`.

## Data

The files used for processing are downloaded from the DDBJ ftp server.

<https://ddbj.nig.ac.jp/public/ddbj_database/bioproject/bioproject.xml>

and

<https://ddbj.nig.ac.jp/public/ddbj_database/biosample/biosample_set.xml.gz>