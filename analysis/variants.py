"""
Variation Detection Script
--------------------------

This script is designed to identify and calculate variations in terms 
extracted from the DDBJ using various distance metrics. It 
integrates with MongoDB for data retrieval, employs parallel processing for 
efficiency, and outputs results in a structured format for further analysis.

Features:
- Load and preprocess variant data.
- Calculate tag similarities using distance metrics such as:
  - Cosine Similarity
  - Jaro Similarity
  - Jaro-Winkler Similarity
  - Levenshtein Distance
  - Damerau-Levenshtein Distance
- Use MongoDB aggregation pipelines to extract relevant data.
- Multithreaded processing for improved performance.

Requirements:
- Python 3.x
- MongoDB server running locally
- Required Python libraries:
  - pandas
  - pymongo
  - distance_calculation (custom module)

Usage:
1. Ensure MongoDB is running contains the required collections.
2. Place the input variants JSON file in the `data` folder.
3. Run the script using `python <script_name>.py`.

Configuration:
- Logging: Output is saved in the `logs` directory.
- MongoDB connection details and thresholds can be adjusted in the code.

Author: [Your Name]
Date: [YYYY-MM-DD]
"""


# Variation detection
import hashlib
import json
import logging
import os
import queue
import re
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

import pandas as pd
import pymongo

from distance_calculation import calculate_damerau_levenshtein_distance, calculate_levenshtein_distance, \
    jaro_winkler_similarity, jaro_similarity, calculate_cosine_similarity

# Importing libraries

# Global variables
variants = {}
terms = {}
ignores = set()
output_folder = Path("data")
# Minimum threshold for the number of occurrences for a tag
THRESHOLD = 10

# Specify the number of threads
num_threads = 20  # Adjust this number based on your requirements

# MongoDB connection
client = pymongo.MongoClient('mongodb://myTester:jasper@localhost:27017/', connect=True)
print(client.server_info())


def get_variants():
    return variants


def get_terms():
    return terms

def get_ignores():
    return ignores


def set_logging(name: str = "determine_variants_by_hand", debug: bool = False) -> None:
    """
    Set the logging configuration.
    """
    import logging, os

    # Create a custom logger
    logger = logging.getLogger()
    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    # Create handlers
    if not os.path.exists("logs"):
        os.makedirs("logs")

    file_handler = logging.FileHandler(Path("logs") / f"{name}.log")
    if debug:
        file_handler.setLevel(logging.DEBUG)
    else:
        file_handler.setLevel(logging.INFO)

    console_handler = logging.StreamHandler()

    if debug:
        console_handler.setLevel(logging.DEBUG)
    else:
        console_handler.setLevel(logging.INFO)

    # Create formatters and add them to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    console_handler.setFormatter(formatter)

    # Add handlers to the logger
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)

    # Forcefully enable logging to all handlers
    if debug:
        logging.basicConfig(level=logging.DEBUG, force=True)
    else:
        logging.basicConfig(level=logging.INFO, force=True)

    # Example usage
    logger.debug('This is a debug message')
    logger.info('This is an info message')
    logger.warning('This is a warning message')
    logger.error('This is an error message')
    logger.critical('This is a critical message')


def load_variants(file_path: Path = Path("data") / "variants.json") -> None:
    # TODO obtain all terms from NCBI / EBI
    # TODO obtain all terms from MIXS

    # Load data from file
    global variants
    variants = json.loads(open(file_path, "r").read())

    # Term lookup
    for term in variants:
        # print(term, variants[term]['true_variants'])
        variant_options = ['true_variants', 'false_variants']
        for option in variant_options:
            if term == 'ignore':
                global ignores
                ignores = variants[term]['tags']
                continue
            for index, v in enumerate(variants[term][option]):
                # Lowercase, strip, replace underscores and dashes
                v = v.lower().strip().replace("_", " ").replace("-", " ")
                # Remove multiple spaces
                v = re.sub(r'\s+', ' ', v)
                if v == term:
                    variants[term][option][index] = None
                else:
                    variants[term][option][index] = v
            variants[term][option] = list(set(variants[term][option]))
            # Remove None values
            variants[term][option] = list(filter(None, variants[term][option]))
    # Write to file
    with open(Path("data") / "variants_corrected.json", "w") as f:
        f.write(json.dumps(variants, indent=4))
    # Obtain all terms to reduce further computation by using lookup
    global terms
    for term in variants.keys():
        terms[term] = term
    # Obtain all true variants
    for term in variants.keys():
        if 'true_variants' not in variants[term]: continue
        for term_variant in variants[term]['true_variants']:
            terms[term_variant] = term
    print(f"Loaded {len(terms)} terms")


def calculate_distance(single_tag: str, list_of_tags: [str], abundance: int) -> None:
    """
    Calculate the distances between the tags of the two lists.

    :param single_tag: str, the tag to compare the list of tags with
    :param list_of_tags: list(str), the tags to compare the single tag with
    :param output_folder: str, the location of the output folder
    :param metrics: dict(str: Callable), the distance metrics to use
    :return: None
    """

    metrics = {
        "Cosine-similarity": calculate_cosine_similarity,
        "Jaro": jaro_similarity,
        "Jaro-Winkler": jaro_winkler_similarity,
        "Levenshtein": calculate_levenshtein_distance,
        "Damerau-Levenshtein": calculate_damerau_levenshtein_distance
    }

    for metric_name, distance_function in metrics.items():
        md5_single_tag = hashlib.md5(single_tag.encode()).hexdigest()
        output_file_location = (output_folder / "distance" / f"{md5_single_tag}_{metric_name}.tsv")
        if not output_file_location.parent.exists():
            os.makedirs(output_file_location.parent)
        if output_file_location.exists():
            logging.debug(f"File already exists: {output_file_location}")
            continue
        logging.debug(f"Start calculating the distances for {single_tag}")

        # Initialize a list to accumulate rows
        rows = []

        for tag_from_list in list_of_tags:
            simmilarity = distance_function(str(single_tag).lower(), str(tag_from_list).lower())
            simmilarity = round(simmilarity, 4)
            if simmilarity > 0.5:
                rows.append({"tag1": single_tag, "tag2": tag_from_list, "distance": simmilarity, "abundance": abundance})

        # Create a DataFrame from the accumulated rows
        df = pd.DataFrame(rows, columns=["tag1", "tag2", "distance", "abundance"])
        # Somehow we have duplicates, so we need to remove the duplicate rows
        df = df.drop_duplicates()

        # Sort the dataframe by distance
        df = df.sort_values(by="distance", ascending=False)

        # Write the dataframe to a file
        df.to_csv(output_file_location, sep="\t", index=False)


def obtain_all_tags():
    # Mongo connection to obtain all tags
    db = client['bio']
    # collection = db['ddbj_samples']
    collection = db['ncbi_samples']

    # Create an index on attribute_name
    logging.info("Creating index on attribute_name")
    collection.create_index("BioSample.Attributes.Attribute.attribute_name")
    logging.info("Index created")

    # Count the number of documents in the collection
    document_count = collection.estimated_document_count()

    # Print the result
    print(f"Number of documents in the collection: {document_count}")

    # Define the aggregation pipeline with a limit
    pipeline = [
        # {
            # "$skip": 100_000_000  # Skip the first xxx records (set your offset here)
        # },
        {
            "$limit": 100_000  # Limit the aggregation to the first 1,000,000 records
        },
        {
            "$unwind": "$BioSample.Attributes.Attribute"
        },
        {
            "$group": {
                "_id": "$BioSample.Attributes.Attribute.attribute_name",
                "tagCount": {"$sum": 1}
            }
        },
        {
            "$project": {
                "_id": 0,
                "attribute_name": "$_id",
                "count": "$tagCount"
            }
        },
        {
            "$sort": {"count": -1}
        }
    ]

    print("Starting the aggregation pipeline")
    # Execute the aggregation pipeline
    attribute_results = list(collection.aggregate(pipeline))
    # Only keep the attributes that have a count of more than THRESHOLD
    attribute_results = [result for result in attribute_results if result['count'] > THRESHOLD]

    task_queue = queue.Queue()

    def monitored_calculate_distance(attribute_name: str, terms: [str], count: int) -> None:
        try:
            # Execute the calculate_distance function
            calculate_distance(attribute_name, terms, count)
        finally:
            # Decrement the task count when the job is done
            task_queue.get()
            if task_queue.qsize() % 10 == 0 or task_queue.qsize() < 10:
                logging.info(f"Task done: {task_queue.qsize()}")

    # Using ThreadPoolExecutor for parallel processing
    with ThreadPoolExecutor(max_workers=num_threads) as executor:
        for index, attribute in enumerate(attribute_results):
            # Clean the attribute name
            attribute_name = str(attribute['attribute_name']).lower().strip().replace("_", " ").replace("-", " ")
            # Check if the attribute is in the list of terms
            if attribute_name not in terms:
                logging.debug(f"Attribute not in terms: {attribute_name}")
                task_queue.put(attribute_name)
                # Compute the similarity
                executor.submit(monitored_calculate_distance, attribute_name, terms, attribute['count'])
    logging.info("All tasks submitted")


def main():
    set_logging("data", debug=False)
    load_variants()
    obtain_all_tags()


if __name__ == '__main__':
    main()
